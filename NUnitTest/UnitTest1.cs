﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;

namespace NUnitTest
{
    [TestFixture]
    public class UnitTest1
    {

        [SetUp]
        public void Setup()
        {
        }
        [Test]
        public void TestMain()
        {
            String Expected = "Hello World!";
            using (var sw = new StringWriter())
            {
                Console.SetOut(sw);
                NUnitSample.Program.Main();
                var result = sw.ToString().Trim();
                NUnit.Framework.Assert.AreEqual(Expected, result);
            }
        }
        [Test]
        public void TestIsOdd()
        {
            bool Expected = false;
            var isodd = NUnitSample.Program.isOdd(10);

            NUnit.Framework.Assert.AreEqual(Expected, isodd);
        }
    }
}
